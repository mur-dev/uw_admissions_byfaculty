(function ($) {

Drupal.behaviors.exampleModule = {
	attach: function (context, settings) {
		switch($("#edit-studenttype").val()){
			case "1":
				$("#international").hide();
				$("#canadian").show();
				$("#programs").show();
				$("#nonTypicalStudentTypes").hide();
				break;
			case "2":
				$("#international").show();
	 			$("#canadian").hide();
				$("#programs").show();
				$("#nonTypicalStudentTypes").hide();
				break;
			case "3":
				$("#international").hide();
	 			$("#canadian").hide();
				$("#programs").hide();
				$("#nonTypicalStudentTypes").show();
				break;
			case "4":
				$("#international").hide();
				$("#canadian").hide();
				$("#programs").show();
				$("#nonTypicalStudentTypes").hide();
				break;		
		}
		switch($('input:radio[name=system]:checked').val()){
			case "1000":
				$(".form-item-country").show();
				break;
			default:
				$(".form-item-country").hide();
		}
		$("#edit-studenttype").change(function (){
			switch($(this).val()){
				case "1":
					$("#international").hide();
					$("#canadian").show();
					$("#programs").show();
					$("#nonTypicalStudentTypes").hide();
					break;
				case "2":
					$("#international").show();
					$("#canadian").hide();
					$("#programs").show();
					$("#nonTypicalStudentTypes").hide();
					break;
				case "3":
					$("#international").hide();
					$("#canadian").hide();
					$("#programs").hide();
					$("#nonTypicalStudentTypes").show();
					break;
				case "4":
					$("#international").hide();
					$("#canadian").hide();
					$("#programs").show();
					$("#nonTypicalStudentTypes").hide();
					break;
			}
		});
		$(".form-radio").click(function (){
			switch($(this).val()){
				case "1000":
					$(".form-item-country").show();
					break;
				default:
					$(".form-item-country").hide();
			}
		});
	}
};
})(jQuery);
